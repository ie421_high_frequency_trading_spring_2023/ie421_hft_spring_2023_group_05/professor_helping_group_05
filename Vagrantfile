Vagrant.configure("2") do |config|

  config.vm.define "pxeserver" do |pxeserver|
    pxeserver.vm.box = "bento/fedora-latest"
    pxeserver.vm.network "private_network", type: "static", ip: "192.168.56.3",  mac: "DEADBEEF0001"
    pxeserver.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.cpus = 4

      # Disable DHCP for vboxnet0
      #vb.customize ["dhcpserver", "remove", "--ifname", "vboxnet0"]
    end
    pxeserver.vm.provision "shell", inline: <<-SHELL
      echo 'SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="de:ad:be:ef:00:01", NAME="eth1"' | sudo tee /etc/udev/rules.d/70-persistent-net.rules

      echo -e "[connection]\n\
id=eth1\n\
uuid=$(uuidgen)\n\
type=ethernet\n\
interface-name=eth1\n\
[ipv4]\n\
method=manual\n\
addresses=192.168.56.3/24\n\
[ipv6]\n\
method=disabled" | sudo tee /etc/NetworkManager/system-connections/eth1.nmconnection

      sudo chmod 600 /etc/NetworkManager/system-connections/eth1.nmconnection
      sudo systemctl restart NetworkManager      
      sudo nmcli con up eth1
      
      #stop and perm disable firewall from starting on reboot
      sudo systemctl stop firewalld && systemctl disable firewalld

      #below install is just for troubleshooting, not required to actually run
      sudo dnf install -y tcpdump nano --disablerepo=* --enablerepo=fedora --enablerepo=updates

      #below is required
      sudo dnf install -y dnsmasq nfs-utils syslinux-tftpboot syslinux dracut-network --disablerepo=* --enablerepo=fedora --enablerepo=updates

      echo -e 'dhcp-range=192.168.56.100,192.168.56.254,255.255.255.0,12h' >> /etc/dnsmasq.conf
      echo -e 'dhcp-host=52:54:00:55:66:77,192.168.56.4' >> /etc/dnsmasq.conf
      echo -e 'interface=eth1' >> /etc/dnsmasq.conf
      echo -e 'enable-tftp' >> /etc/dnsmasq.conf
      echo -e 'tftp-root=/var/ftpd' >> /etc/dnsmasq.conf
      #echo -e 'pxe-service=x86PC, "Boot from network", fake-file' >> /etc/dnsmasq.conf
      echo -e 'log-facility=/var/log/dnsmasq.log' >> /etc/dnsmasq.conf
      echo -e 'log-queries' >> /etc/dnsmasq.conf
      echo -e 'log-dhcp' >> /etc/dnsmasq.conf
      echo -e '# PXE menu\npxe-prompt="Press F8 for boot menu", 1\npxe-service=x86PC, "Boot from network", pxelinux\n# NFS root settings for the client\ndhcp-option=17,192.168.56.3:/srv/nfsroot' >> /etc/dnsmasq.conf

      sudo mkdir -p /var/ftpd
      sudo chown nobody:nobody /var/ftpd
      
      echo "Finished installed dnsmasq; starting now!"
      sudo systemctl start dnsmasq

      echo "Configuring NFS and diskless root filesystem"
      #sudo dnf -y install -y nfs-utils syslinux-tftpboot syslinux dracut-network
      sudo mkdir -p /srv/nfsroot
      echo "/srv/nfsroot 192.168.56.0/24(rw,sync,no_root_squash,no_subtree_check)" | sudo tee -a /etc/exports
      sudo systemctl enable nfs-server --now

      sudo dnf -y --installroot=/srv/nfsroot --releasever=38 install @core
      VMLINUZ_FILE=$(ls /boot/vmlinuz-* | grep -v "rescue" | sort -V | tail -n 1)
      INITRAMFS_FILE=$(ls /boot/initramfs-*.img | grep -v "rescue" | sort -V | tail -n 1)

      # Copy the files to the destination
      sudo cp "$VMLINUZ_FILE" /var/ftpd/vmlinuz
      sudo cp "$INITRAMFS_FILE" /var/ftpd/initrd.img
    

      #copy pxelinux.0
      sudo cp /usr/share/syslinux/pxelinux.0 /var/ftpd/
      sudo cp /usr/share/syslinux/ldlinux.c32 /var/ftpd/
      
      dracut --add "nfs network ssh-client ifcfg network-manager usrmount" -f /var/ftpd/initramfs-6.2.11-300.fc38.x86_64.img 6.2.11-300.fc38.x86_64
      chmod 644 /var/ftpd/initramfs-6.2.11-300.fc38.x86_64.img

#
      sudo chroot /srv/nfsroot /bin/bash -c "echo 'root:vagrant' | chpasswd"
      sudo chroot /srv/nfsroot /bin/bash -c "groupadd vagrant && useradd -m -g vagrant -p '*' vagrant && echo -e 'vagrant\nvagrant' | passwd vagrant"


      sudo mkdir /var/ftpd/pxelinux.cfg
      sudo sh -c 'echo -e "DEFAULT linux\n\nLABEL linux\n    KERNEL vmlinuz\n    APPEND initrd=initramfs-6.2.11-300.fc38.x86_64.img ip=dhcp selinux=0 root=/dev/nfs nfsroot=192.168.56.3:/srv/nfsroot rw" > /var/ftpd/pxelinux.cfg/default'
      sudo chown -R nobody:nobody /var/ftpd/pxelinux.cfg/
      sudo chmod +r /var/ftpd/initrd.img

    SHELL
  end

  config.vm.define "disklessclient" do |disklessclient|
    disklessclient.vm.box = "bento/fedora-latest"
    disklessclient.vm.network "private_network", type: "static", ip: "192.168.56.4", mac: "525400556677"
    disklessclient.vm.provider "virtualbox" do |vb|
      vb.gui = true
      
      # Adjust the boot order to prioritize network boot from the second NIC
      vb.customize ["modifyvm", :id, "--boot1", "net"]
      vb.customize ["modifyvm", :id, "--boot2", "none"]
      vb.customize ["modifyvm", :id, "--boot3", "none"]
      vb.customize ["modifyvm", :id, "--boot4", "none"]
      
      # Ensure that the first NIC doesn't try to boot via PXE
      # set the first NIC (the default one created by vagrant used to ssh into host by default) to lowest priority - this may not be working though as it still does DHCP on that one first
      vb.customize ["modifyvm", :id, "--nic-boot-prio1", "4"]

      #set the 2nd NIC created for PXE boot to highest priority
      vb.customize ["modifyvm", :id, "--nic-boot-prio2", "1"]

      #TODO: check if VBoxManage command for disabling NIC1 ROM option (what tells it to try to do PXE boot on start up; on bare metal this is almost always an option in the BIOS)
      
      #this can be removed later as I never actually bothered looking through this
      vb.customize ["modifyvm", :id, "--bios-pxe-debug=on"]
    end
  end

end

